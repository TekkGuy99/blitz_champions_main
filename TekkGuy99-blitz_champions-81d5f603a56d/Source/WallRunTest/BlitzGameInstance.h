// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "UI/MenuInterface.h"

#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"

#include "BlitzGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class WALLRUNTEST_API UBlitzGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	UBlitzGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init();

	UFUNCTION(BlueprintCallable)
		void loadMenu();

	UFUNCTION(BlueprintCallable)
		void loadPauseMenu();

	UFUNCTION(exec)
		void host() override;

	UFUNCTION(exec)
		void join(uint32 index) override;

	void refreshServerList() override;

	virtual void loadMainMenu() override;

private:
	TSubclassOf<class UUserWidget> MenuClass;
	TSubclassOf<class UUserWidget> PauseMenuClass;
	class UMainMenu* menu;

	IOnlineSessionPtr sessionInterface;
	TSharedPtr<class FOnlineSessionSearch> sessionSearch;		//creates a shared pointer

	void onCreateSession(FName sessionName, bool success);
	void onDestroySession(FName sessionName, bool success);
	void onFindSession(bool success);
	void onJoinSession(FName sessionName, EOnJoinSessionCompleteResult::Type result);

	void createSession();
};