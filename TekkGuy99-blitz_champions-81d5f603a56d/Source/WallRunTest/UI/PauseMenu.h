// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "PauseMenu.generated.h"

/**
 * 
 */
UCLASS()
class WALLRUNTEST_API UPauseMenu : public UMenuWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* ResumeGameButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* QuitTitleButton;

	UFUNCTION()
		void resumeGame();

	UFUNCTION()
		void quitGameTitle();
};
