// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class WALLRUNTEST_API UMainMenu : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UMainMenu(const FObjectInitializer& ObjectInitializer);

	void setServerList(TArray<FString> serverNames);	//creates an array of strings that store visible servers

	void selectIndex(uint32 index);

protected:
	virtual bool Initialize();

private:
	TSubclassOf<class UUserWidget> ServerFieldClass;

	UPROPERTY(meta = (BindWidget))
		class UButton* HostButton;	//allows "HostButton" UI element to be called within C++

	UPROPERTY(meta = (BindWidget))
		class UButton* JoinButton;	//allows "JoinButton" UI element to be called within C++

	UPROPERTY(meta = (BindWidget))
		class UButton* SearchButton;	//allows "SearchButton" UI element to be called within C++

	UPROPERTY(meta = (BindWidget))
		class UPanelWidget* ServerList;

	UFUNCTION()
		void hostGame();

	UFUNCTION()
		void joinGame();

	UFUNCTION()
		void searchGame();

	TOptional<uint32> selIndex;		//data type where isSet() is true, getValue() is valid
};
