// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class WALLRUNTEST_API IMenuInterface
{
	GENERATED_BODY()

public:
	virtual void host() = 0;	//empty function
	virtual void join(uint32 index) = 0;
	virtual void loadMainMenu() = 0;

	virtual void refreshServerList() = 0;
};
